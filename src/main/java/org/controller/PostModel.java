
package org.controller;

import io.smallrye.common.constraint.NotNull;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;
import org.eclipse.microprofile.openapi.annotations.media.Schema;


public class PostModel {
    
    public Integer id;
    @NotNull
    @Schema(required=true,example="Blabla")
    public String title;
    @NotNull
    @Schema(required=true,example="post 1")
    public String content;
    public String[] tags;

    public PostModel() {
        
    }
    
    
    
    
}
