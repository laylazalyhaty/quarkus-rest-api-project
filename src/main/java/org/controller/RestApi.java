package org.controller;

import io.smallrye.common.constraint.NotNull;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import org.domain.Post;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.ArrayUtils;
import org.domain.Post_Tag;
import org.domain.Tag;
//import org.hibernate.collection.internal.PersistentSet;
import org.jboss.resteasy.annotations.jaxrs.PathParam;



@Path("/post")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RestApi {
    
    @Inject
    EntityManager em;
     
    @GET
    public Response getListPost() {
        
        List<Post> post =Post.findAll().list();
        return Response.ok(post).build();
        
    }
    
    @GET
    @Path("{id}")
    public Response firstResult(@PathParam Integer id) {
        
        Post post = Post.findById(id);
        return Response.ok(post).build();
    }
    
    @POST
    @Path("/save")
    @Transactional
    public Response save(@NotNull PostModel model){        
        try {
            
            // menghapus duplikat data pada json post
            model.tags = new HashSet<String>(Arrays.asList(model.tags)).toArray(new String[0]);
            
            //id post dimulai dari 1
            int id_p = (int) ((int) Post.count() != 0 ? Post.count() + 1 : 1);
            
            Post post= new Post(id_p, model.title, model.content, model.tags);
            post.persist();

            // menambahkan label dari post ke dalam tag dan PostTag secara otomatis
            for (String tampilData:model.tags){
                //id tag&posttag dimulai dari 1
//                int id_t = (int) ((int) Tag.count() != 0 ? Tag.count() + 1 : 1);
//                int id_pt = (int) ((int) Post_Tag.count() != 0 ? Post_Tag.count() + 1 : 1);
                
                
                // inisialisasi temp variabel baru buat nampung array update
               
                String[] insertTags = new String[model.tags.length];
                Tag id_tag = null;
                for(int i=0; i<model.tags.length; i++){
                    Tag tagGet = Tag.findByLabeltag(model.tags[i]);
                    if(tagGet == null){
                        insertTags[i] = model.tags[i];
                        
                        int id_a = (int) ((int) Tag.count() != 0 ? Tag.count() + 1 : 1);
                        // melakukan insert label baru ke tabel tag
                        Tag tag = new Tag(id_a, model.tags[i],"");
                        tag.persist();
                        
                        int id_pa = (int) ((int) Post_Tag.count() != 0 ? Post_Tag.count() + 1 : 1);
                        Post_Tag postTag = new Post_Tag(id_pa, post.getId(), tag.getId());
                        postTag.persist();
                   }                        
              }
               
                
            }
            
            return Response.ok(post).build();
        } catch(ExceptionInInitializerError error){
            return Response.ok(error.getMessage()).build();
        }        
        
    }
    
    @PUT
    @Path("{id}")
    @Transactional
    public Response update(@PathParam Integer id,@NotNull PostModel model){
        
        // menghapus duplikat data pada json post
        model.tags = new HashSet<String>(Arrays.asList(model.tags)).toArray(new String[0]);
        
        Post post= Post.findById(id);
        
        if(post==null){
            throw new WebApplicationException("Post with this Id doesn't exist!",404);
        }
        
        // inisialisasi Tag dan PostTag
        Tag tag = null;
        Tag id_tag = null;
        Post_Tag post_Tag = null;
        
        // inisialisasi temp variabel baru buat nampung array update
        String[] updateTags = new String[model.tags.length];

        for(int i=0; i<model.tags.length; i++){
            Tag tagGet = Tag.findByLabeltag(model.tags[i]);
            if(tagGet == null){
                
                // menambahkan isi array ke temp variabel update
                updateTags[i] = model.tags[i];
                
                // melakukan insert label baru ke tabel tag
                int id_t = (int) ((int) Tag.count() != 0 ? Tag.count() + 1 : 1);
                tag = new Tag(id_t, model.tags[i], "");
                tag.persist();
                
                // menacari data posttag berdasarkan tag label dan diubah ke isi yang baru di tambahkan
                post_Tag = Post_Tag.findByIdTag(id_tag.id);
                post_Tag.id_tag = tag.id;
//                post_Tag.persist();
            } else {
                id_tag = Tag.findByLabeltag(model.tags[i]);
                // menambahkan isi array ke temp variabel update
                updateTags[i] = model.tags[i];
            }
        }
        
        post.title = model.title;
        post.content = model.content;
        
        // mengganti isi kolom tags pada post dengan temp variable array
        post.tags = updateTags;
        
        return Response.ok(post).build();
    }
    
    @DELETE
    @Path("{id}")
    @Transactional
    public Response delete(@PathParam Integer id,@NotNull PostModel model){
        
        Post post= Post.findById(id);
        
        if(post==null){
            throw new WebApplicationException("Post with this Id doesn't exist!",404);
        }
        post.delete();
        
        return Response.ok("Delete succesfully!").build();
    }


}


