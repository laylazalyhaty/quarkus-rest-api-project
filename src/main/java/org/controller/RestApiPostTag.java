
package org.controller;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.domain.Post_Tag;

@Path("/posttag")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RestApiPostTag {
    
    
    @GET
    public Response getListPostTag() {
        
        List<Post_Tag> posttag =Post_Tag.findAll().list();
        return Response.ok(posttag).build();
        
    }
}
