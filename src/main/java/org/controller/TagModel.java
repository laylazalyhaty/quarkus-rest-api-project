
package org.controller;

import io.smallrye.common.constraint.NotNull;
import org.eclipse.microprofile.openapi.annotations.media.Schema;


public class TagModel {
    
    public Integer id;
    @NotNull
    @Schema(required=true,example="Kerja")
    public String label;
    public String posts;

    public TagModel() {
    }
    
    

}
