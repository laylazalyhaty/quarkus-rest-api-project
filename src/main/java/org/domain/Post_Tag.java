
package org.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import static io.quarkus.hibernate.orm.panache.PanacheEntityBase.find;
import io.smallrye.common.constraint.NotNull;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "post_tag", schema = "public")
public class Post_Tag extends PanacheEntityBase {
     
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Column(name="id")
    public Integer id;
    
    @JoinColumn(name = "id_post", referencedColumnName = "id")
    public Integer id_post;
    
    @JoinColumn(name = "id_tag", referencedColumnName = "id")
    public Integer id_tag;

    public Post_Tag() {
    }
    
    public Post_Tag(Integer id, Integer id_post, Integer id_tag) {
        this.id = id;
        this.id_post = id_post;
        this.id_tag = id_tag;
    }

    public Post_Tag(Integer id_tag) {
        this.id_tag = id_tag;
    }
    
    public static Post_Tag findByIdTag(int id_t)
    {
        return find("id_tag", id_t).firstResult();
    }
   
    public static Post_Tag findByIdPost(int id_p)
    {
        return find("id_post", id_p).firstResult();
    }

    public void setId_tag(Integer id_tag) {
        this.id_tag = id_tag;
    }    

    public Integer getId_post() {
        return id_post;
    }

    public Integer getId_tag() {
        return id_tag;
    }    
     
    
}
