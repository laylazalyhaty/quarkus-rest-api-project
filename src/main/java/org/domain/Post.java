
package org.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.smallrye.common.constraint.NotNull;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.yaml.snakeyaml.util.ArrayUtils;



@Entity
@Table(name = "Post", schema = "public")
public class Post extends PanacheEntityBase {
    
    
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Column(name="id")
    public Integer id;
    @NotNull
    @Column(name="title")
    public String title;
    @NotNull
    @Column(name="content")
    public String content;
    @Column(name="tags")
    public String[] tags;

    public Post() {
    }

    public Post(Integer id, String title, String content, String[] tags) {
        this.id = id;
        this.title = title;
        this.content = content;        
        this.tags = tags;
    }

    public Integer getId() {
        return id;
    }
    
   
}
