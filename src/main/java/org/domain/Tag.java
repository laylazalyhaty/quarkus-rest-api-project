
package org.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import static io.quarkus.hibernate.orm.panache.PanacheEntityBase.list;
import io.smallrye.common.constraint.NotNull;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "tag", schema="public")
public class Tag extends PanacheEntityBase{
    
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Column(name="id")
    public Integer id;
    @NotNull
    @Column(name="label")
    public String label;
    @Column(name="posts")
    public String posts;

    public Tag() {
    }

    public Tag(Integer id, String label, String posts) {
        this.id = id;
        this.label = label;
        this.posts = posts;
    }

    public Tag(String label, String posts) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Integer getId() {
        return id;
    }
    
    public static Tag findByLabeltag(String label)
    {
        return find("label", label).firstResult();
    }

    public String getLabel() {
        return label;
    }
    
    

    
  

 
    
}
